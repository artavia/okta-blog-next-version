// =============================================
// ADDITIONAL BOILERPLATE 
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
} 

const { 
  
  NODE_ENV
  
  , OKTA_ORG_URL
  , OKTA_CLIENT_ID
  , OKTA_CLIENT_SECRET
  , OKTA_TESTING_DOMAIN // NEW for use with @okta/oidc-middleware@latest

} = process.env;


const { ExpressOIDC } = require("@okta/oidc-middleware");

// okta OIDC -- NEW 4.x @latest VERSION
const oidcObj = {
  issuer: `${OKTA_ORG_URL}/oauth2/default`
  
  , appBaseUrl: OKTA_TESTING_DOMAIN // NEW 

  , client_id: OKTA_CLIENT_ID
  , client_secret: OKTA_CLIENT_SECRET
  
  // , loginRedirectUri: "http://localhost:3000/authorization-code/callback"  // NEW 
  , loginRedirectUri: `${OKTA_TESTING_DOMAIN}/authorization-code/callback`  // NEW 

  , scope: "openid profile"
  , routes: {
    login: {
      path: "/users/login"
    }
    , loginCallback: {
      path: "/authorization-code/callback" // NEW 
      , afterCallback: "/dashboard"
    }
  }
  , ensureAuthenticated: ( req, res, next ) => {
    if(!req.userContext){
      return res.status(401).render("unauthenticated");
    }
    next();
  }
  
};
const oidc = new ExpressOIDC( oidcObj );

module.exports = { oidc };

// SEE THIS...
// SEE https://github.com/okta/okta-oidc-js/tree/master/packages/oidc-middleware#new-expressoidcconfig

// AND, SEE THIS...
// https://github.com/okta/okta-oidc-js/tree/master/packages/oidc-middleware#customizing-routes