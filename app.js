// =============================================
// ADDITIONAL BOILERPLATE 
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
} 

const { 
  
  NODE_ENV

  , EXPRESS_SESSION_SECRET

  , SESS_NAME
  , SESS_LIFETIME = 1000 * 60 * 60 * 2 // 2 HOURS

} = process.env;

const IN_PROD = NODE_ENV === 'production';


const createError = require('http-errors');
const express = require('express');

const path = require('path');

// const cookieParser = require('cookie-parser');

const logger = require('morgan');

const session = require("express-session");

const {oidc} = require("./oidc_object");

const auth = require("./auth");
const { client } = auth;

const blogRouter = require("./routes/blog");
const usersRouter = require('./routes/users');

// App initialization
const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// middleware
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

const sessionConfigObj = {
  
  name: SESS_NAME
  
  , secret: EXPRESS_SESSION_SECRET
  , saveUninitialized: false
  
  , resave: false
  
  , cookie: {
    maxAge: SESS_LIFETIME
  // , sameSite: true // 'strict'
  , sameSite: false
  // , secure: IN_PROD
  }
  
}; // session config NEW
app.use( session( sessionConfigObj ) );

app.use( oidc.router );

// SEE https://github.com/okta/okta-oidc-js/tree/master/packages/oidc-middleware#extending-the-user
// NEW - NEXT - 4.x
const addUserContext = (req, res, next) => {

  if( !req.userContext ){
    return next();
  }
  
  // auth.client.getUser(req.userContext.userinfo.sub)
  client.getUser( req.userContext.userinfo.sub )
  .then( user => {
    req.user = user;
    res.locals.user = user;
    next();
  } );
  
};

app.use( addUserContext );

// routes
app.use('/', blogRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
