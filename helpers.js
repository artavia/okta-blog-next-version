const { Sequelize } = require("sequelize");

const configObj = {
  dialect: "sqlite"
  , storage: "./database.sqlite3"
};

const db = new Sequelize( configObj );

// Return a DB instance
const getDB = () => {
  return db;
};

module.exports = { getDB };