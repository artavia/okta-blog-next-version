const { DataTypes } = require("sequelize");

const helpers = require("./helpers");

const db = helpers.getDB();

const Post = db.define( "posts" , {
  title: {
    type: DataTypes.STRING
  }
  , body: {
    type: DataTypes.TEXT
  }
  , authorId: {
    type: DataTypes.STRING
  }
  , slug: {
    type: DataTypes.STRING
  }
} );

db.sync();

module.exports = { Post };