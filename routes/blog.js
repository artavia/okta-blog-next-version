const async = require("async");

const auth = require("./../auth");
const { client } = auth;

const {oidc} = require("./../oidc_object"); // console.log( "oidc: " , oidc );

const models = require("./../models");
const { Post } = models;

const { Sequelize } = require("sequelize");

const slugify = require("slugify");

const express = require("express");
const router = express.Router();

router.use( oidc.router );

// Render the home page and list all blog posts
router.get("/", (req, res, next)  => { 

  Post.findAll( {
    order: Sequelize.literal("createdAt DESC")
  } )
  .then( posts  => {
    
    let postData = []; 
    
    async.eachSeries( posts , ( post, callback ) => {
      
      post = post.get( { plain: true } ); // console.log( ">>>>>> post:" , post );
      
      client.getUser( post.authorId )
      .then( user => {

        // console.log( ">>>>>> user:" , user );
        
        postData.push( {
          title: post.title
          , body: post.body
          , createdAt: post.createdAt
          , authorName: `${ user.profile.firstName } ${ user.profile.lastName }`
          , slug: post.slug
        } );

        callback();

      } )
      .catch( err => {
        
        postData.push( {
          title: post.title
          , body: post.body
          , createdAt: post.createdAt
          , slug: post.slug
        } ); 

        callback();

      } );

    } , err => { 
      return res.render("index", { posts: postData } );
    } );

  } );

});

// Render the user dashboard
router.get("/dashboard", oidc.ensureAuthenticated(), (req, res, next)  => {
  
  // console.log( ">>>>>>>>> req.user: \n\n", req.user ); 
  
  const { id } = req.user;

  Post.findAll( {
    where: {
      authorId: id
    }
    , order: Sequelize.literal("createdAt DESC")
  } )
  .then( posts  => {
    let postData = [];
    
    posts.forEach( post => {
      postData.push( post.get( { plain: true } ) );
    } );

    return res.render( "dashboard" , { posts: postData } );
  } );

});

// Create a new post
router.post("/dashboard", oidc.ensureAuthenticated(), (req, res, next)  => {

  const { title, body } = req.body;
  const { id } = req.user;

  Post.create( {
    title: title
    , body: body
    , authorId: id
    , slug: slugify( title ).toLowerCase()
  } )
  .then( ( newPost ) => {

    Post.findAll( {
      where: {
        authorId: id
      }
      , order: Sequelize.literal("createdAt DESC")
    } )
    .then( posts => {
      
      let postData = [];

      posts.forEach( post => {
        postData.push( post.get( { plain: true } ) );
      } );

      res.render( "dashboard" , { post: newPost , posts: postData } );

    } );

  } );

});

// Render the edit post page
router.get("/:slug/edit", oidc.ensureAuthenticated(), (req, res, next)  => {
  
  const { slug } = req.params;
  const { id } = req.user;

  Post.findOne( {
    where: {
      slug: slug
      , authorId: id
    }
  } )
  .then( ( post ) => {
    
    if(!post){
      return res.render( "error" , { message: "Page not found." , error: { status: 404 } } );
    }

    post = post.get( { plain: true } ); // console.log( ">>>>>> post:" , post );

    client.getUser( post.authorId )
    .then( user => {
      
      // console.log( ">>>>>> user:" , user );

      post.authorName = `${ user.profile.firstName } ${ user.profile.lastName }`;

      res.render( "edit" , { post } );

    } );

  } );

});

// Update a post
router.post("/:slug/edit", oidc.ensureAuthenticated(), (req, res, next)  => {
  
  const { slug } = req.params;
  const { id } = req.user;

  const { title, body } = req.body;

  Post.findOne( {
    where: {
      slug: slug
      , authorId: id
    }
  } )
  .then( post => {
    
    if(!post){
      return res.render( "error" , { message: "Page not found." , error: { status: 404 } } );
    }

    post.update( {
      title: title
      , body: body
      , slug: slugify( title ).toLowerCase()
    } )
    .then( () => {
      
      post = post.get( { plain: true } ); // console.log( ">>>>>> post:" , post );
      
      client.getUser( post.authorId )
      .then( user => {
        
        // console.log( ">>>>>> user:" , user );

        post.authorName = `${ user.profile.firstName } ${ user.profile.lastName }`;

        res.redirect( "/" + slugify( title ).toLowerCase() );

      } );

    } ); 

  } );

});

// Delete a post
router.post("/:slug/delete", oidc.ensureAuthenticated(), (req, res, next)  => {
  
  const { slug } = req.params;
  const { id } = req.user;

  Post.findOne( {
    where: {
      slug: slug
      , authorId: id
    }
  } )
  .then( (post) => {
    
    if(!post){
      return res.render( "error" , { message: "Page not found." , error: { status: 404 } } );
    }

    post.destroy();
    res.redirect( "/dashboard" );

  } );

});

// View a post
router.get("/:slug", (req, res, next)  => {
  
  const { slug } = req.params;

  Post.findOne( {
    where: {
      slug: slug
    }
  } )
  .then( (post) => {
    
    if(!post){
      return res.render( "error" , { message: "Page not found." , error: { status: 404 } } );
    }

    post = post.get( { plain: true } ); // console.log( ">>>>>> post:" , post );

    client.getUser( post.authorId )
    .then( user => {
      
      // console.log( ">>>>>> user:" , user );
      
      post.authorName = `${ user.profile.firstName } ${ user.profile.lastName }`;

      res.render( "post" , { post } );

    } );

  } );

});

module.exports = router;
