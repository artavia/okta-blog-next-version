# Okta Blog Tutorial

## Description
This is the modernized Okta lesson by Randall Degges locked and loaded for use in July 2020. That article is courtesy of the [Okta blog](https://developer.okta.com/blog/2018/06/28/tutorial-build-a-basic-crud-app-with-node "link to Express CRUD app").

### Processing and/or completion date(s)
July 05, 2020 to July 06, 2020

## &quot;What is different?&quot; you ask&hellip;
Hmmm&hellip; lots!!!

In terms of the proprietary software such as those that belong to and are formulated by OktaIn I decided to use the **modern and/or @next versions** inevitably bringing about the **breaking changes** that are guaranteed to materialize. 

By referring to the github README, [especially in terms of **@okta/oidc-middleware**](https://github.com/okta/okta-oidc-js/tree/master/packages/oidc-middleware#new-expressoidcconfig "link to README"), you will find that an additional property named **appBaseUrl** is needed in order to make everything jive. But that is just the beginning!!! The application setup is slightly different in the API portal, too. 

My recommendation is that if you mess around with the **retired versions** and have applications corresponding to those versions, then, it would be essential for you to **create new applications in the portal altogether** if you want to experiment with **the latest (and less brittle) versions of the npm packages** involved.


## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the ApolloGraphQL fullstack tutorial (or, **AFT** for short) on two separate occasions. The authors **purported** that one does not need to know the other minor themes in order to complete the lesson. That would be nice if it were true, however, it was not meant to be so.

When I am fortunate enough to undertake tutorials and lessons by third&#45;parties I usually prefer to hammer them out with the keyboard and go through the motions as opposed to merely blindly copying and pasting code (in addition to consulting API references and guides).

In terms of the **AFT**, it became obvious that some of the technologies were archaic, possibly deprecated &amp; likely containing security vulnerabilities. 

Therefore, in order to follow and ultimately complete the **AFT** to fruition, I decided to embark on learning and/or shoring-up some of the minor themes involved such as Sqlite3 and Sequelize (which I had been meaning to do for quite some time). 

Personally, I think my plan was a success. The problem was never with learning GraphQL and/or ApolloGraphQL. The bugs I was sure at the time were limited to (again) the minor themes. I was familiar with WebSQL for browsers but Sqlite3 would prove to be different. And, I had seen Sequelize from afar when researching other subjects such as MERN and MEAN stacks. I could not procrastinate with learning Sequelize any more. I am thrilled to have finally started practicing with Sequelize. **Yes!!!**


## God bless!
Please help me if you are able. Thank you for your visit. 